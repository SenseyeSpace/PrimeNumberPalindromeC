# Palindrome made from the product of two 5-digit prime numbers

##### Start
```bash
gcc main.c -o prime -lm -O && ./prime
```

##### Result
```
Palindrome: 999949999
Prime numbers: 30109 * 33211
Duration: 11 932 000
```

##### Optimize:
* Ознака ділення на 11 — *Число ділиться на 11, якщо сума цифр на парних місцях мінус сума на непарних ділиться на 11*
* [malloc and gcc optimization](https://stackoverflow.com/questions/17848426/malloc-and-gcc-optimization)

##### Languages:
* [JavaScript](https://gitlab.com/Senseye/PrimeNumberPalindromeJavaScript)
* [Python](https://gitlab.com/Senseye/PrimeNumberPalindromePython)
* [Golang](https://gitlab.com/Senseye/PrimeNumberPalindromeGolang)
* [Rust](https://gitlab.com/Senseye/PrimeNumberPalindromeRust)
