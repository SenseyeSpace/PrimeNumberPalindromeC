#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

typedef struct array {
    int length;
    int * data;
} array;

typedef struct result {
    int palindrome;
    int left;
    int right;
} result;

// Max possible result, all 10-digits palindromes % 11 == 0
int const max = 999999999;
int digits[9];
int limit = 5;
int right = 8;

int isPalindrome(int value)
{
    int i = 0;

    while (value > 0) {
        digits[i] = value % 10;
        i++;

        value /= 10;
    }

    for (int j = 0; j < limit; ++j) {
        if (digits[j] != digits[right-j]) {
            return 0;
        }
    }

    return 1;
}

float now()
{
    return (float)clock()/CLOCKS_PER_SEC;
}

array getPrimeNumbers(int from, int to)
{
    int size = to + 1;

    int *variants = malloc(sizeof(int) * size);

    for (int i = 0; i < size; ++i) {
        variants[i] = 1;
    }

    int limit = ceil(sqrt(to));

    for (int i = 2; i < limit; ++i) {
        if (variants[i]) {
            for (int j = i * i; j < size; j += i) {
                variants[j] = 0;
            }
        }
    }

    int length = 0;

    for (int i = from; i < size; ++i) {
        if (variants[i]) {
            variants[length] = i;
            length += 1;
        }
    }

    return (array){.data = variants, .length = length };
}

result findResult()
{
    array primes = getPrimeNumbers(10000, 99999);

    result current = (result) { .palindrome = 0, .left = 0, .right = 0};
    int palindrome = 0;
    int left = 0;
    int right = 0;

    int size = primes.length;
    int *data = primes.data;

    for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
            int possible = data[i] * data[j];

            if (possible > max) {
                break;
            }

            if (possible > palindrome && isPalindrome(possible)) {
                palindrome = possible;
                left = data[i];
                right = data[j];
            }
        }
    }

    return (result){ .palindrome = palindrome, .left = left, .right = right};
}

int main()
{
    float startTime = now();
    result current = findResult();
    float endTime = now();

    printf("Palindrome: %d\n", current.palindrome);
    printf("Prime numbers: %d * %d\n", current.left, current.right);
    printf("Duration: %0.9f\n", endTime - startTime);

    return 0;
}
